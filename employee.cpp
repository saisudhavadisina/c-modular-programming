#include <iostream>
using namespace std;

class Sample {
public:
    int a;
    float b;
    char c;
    void print()
    {
        cout << "a is "<< a << endl;
        cout << "b is "<< b << endl;
        cout << "c is "<< c << endl;
    }
};

int main(){
    Sample s1;
    Sample *p;
    p = &s1;
    p->a=10;
    p->b=23.56;
    p->c='A';
    p->print();
return 0;
}
