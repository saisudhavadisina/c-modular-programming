package elab;
import java.util.Scanner;
import java.io.*;
import java.text.DecimalFormat;
import java.lang.String;


public class Speed {
	public static void main(String[] args) {
		DecimalFormat df = new DecimalFormat("#.##");
		Scanner sc = new Scanner(System.in);
		int v = sc.nextInt();
		int a = sc.nextInt();
		double x = (double)(v * v) / (2 * a);
		System.out.printf(df.format(x));
	}
}