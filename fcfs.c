#include <stdio.h>

int main(){
    int at[30], bt[30], n;
    int temp = 0, sum = 0, count = 0;
    float tat, wt;
    printf("Enter no.of processes : ");
    scanf("%d", &n);
    printf("Enter arrival times of %d processes : ", n);
    for(int i = 0; i < n; i++)
        scanf("%d", &at[i]);
    printf("Enter Burst times of %d processes : ", n);
    for(int i = 0; i < n; i++)
        scanf("%d", &bt[i]);
    int max = at[0];
    for(int i = 0; i < n; i++)
        if(at[i] > max)
            max = at[i];
    while(temp <= max) {
        for(int i = 0; i < n; i++)
            if(at[i] == temp){
                sum += bt[i];
                printf("Process %d - complete time:%d\t turnaround time:%d\t waiting time:%d\n", i + 1, sum, sum - at[i], sum - at[i] - bt[i]);
                tat += sum - at[i];
                wt += tat - bt[i];
            }
        if(sum < temp + 1)
            sum++;
        temp++;
    }
    printf("Average turnaround time : %0.2f\n", tat / n);
    printf("Average waiting time : %0.2f\n", wt / n);
    return 0;
}
