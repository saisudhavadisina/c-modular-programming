#include<iostream>
#include<iomanip>
#include<stdio.h>
using namespace std;

class ConsoleDemo
{
	public:
	void  getDemo(){
	    cout<<"Enter a character : ";
	    char c=cin.get();
        cout<<c<<endl;
	}
	void  putDemo(){
	    cout<<"Enter a character : ";
	    fflush(stdin);
	    char c=cin.get();
        cout.put(c) << endl; //Here it prints the value of variable c;
        cout.put('c') << endl; //Here it prints the character 'c';
	}
	void  getlineDemo(){
	    cout<<"Enter name : ";
        char c[10];
        fflush(stdin);
        cin.getline(c,10); //It takes 10 characters as input;
        cout<<c<<endl;
	}
	void  writeDemo(){
	    cout<<"Enter text of 10 characters : ";
        char c[10];
        cin.getline(c,10); //It takes 10 characters as input;
        cout.write(c,9) << endl; //It reads only 9 character from buffer c;
	}
	void  setwDemo(){
	    int x=25;
        cout<<setw(10)<<x<<endl;
	}
	void  setfillDemo(){
	    int x=25;
        cout<<setw(10);
        cout<<setfill('#')<<x<<endl;
	}
	void  setprecisionDemo(){
	    float x=10.12345;
        cout<<setprecision(5)<<x;
	}
};

int main()
{
	ConsoleDemo d;
	d.getDemo();
	d.putDemo();
	d.getlineDemo();
	d.writeDemo();
	d.setwDemo();
	d.setfillDemo();
	d.setprecisionDemo();
	return 0;
}
