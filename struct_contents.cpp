#include <iostream>
using namespace std;

struct student
{
  int roll_no;
  string name;
  int phone_number;
};

int main(){

	struct student p1 = {1,"Raju",123443};
  	struct student p2 = {2,"Ramu",123456};
  	struct student p3 = {3,"Siva",123455};

	cout <<"Student 1 details" <<endl;
  	cout << "roll no : " << p1.roll_no << endl;
  	cout << "name : " << p1.name << endl;
  	cout << "phone number : " << p1.phone_number << endl << endl;
  	cout <<"Student 2 details" <<endl;
  	cout << "roll no : " << p2.roll_no << endl;
  	cout << "name : " << p2.name << endl;
  	cout << "phone number : " << p2.phone_number << endl << endl;
  	cout <<"Student 3 details" <<endl;
  	cout << "roll no : " << p3.roll_no << endl;
  	cout << "name : " << p3.name << endl;
  	cout << "phone number : " << p3.phone_number << endl;
	return 0;
}
