#include <stdio.h>
int n;
int main() {
    printf("Enter size of adjacancy matrix:");
    scanf("%d", &n);

    int a[n][n], q[n], v[5] = {0};
    int start, f = -1, r = -1, j;

    printf("Enter adjacancy matrix:");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            scanf("%d", &a[i][j]);
        }
    }

    printf("Enter start :");
    scanf("%d", &start);

    q[++r] = start;
    f++;
    v[start] = 1;
    j = start;

    while (f <= r) {
        printf("%d\t", j);
        for (int p = 0; p < n; p++) {
            if (a[j][p] == 1 && v[p] == 0) {
                q[++r] = p;
                v[p] = 1;
            }
        }
        j = q[++f];
    }

    return 0;
}
