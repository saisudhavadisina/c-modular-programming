#include <iostream>
using namespace std;

class Student {
public:
    string name;
    int rollno;
    char grade;
};

int main(){
    Student s[3];
    for(int i=1;i<=3;i++){
        cout<<"enter the information of student "<<i<<endl;
        cout<<"name : ";
        cin>>s[i-1].name;
        cout<<"rollno : ";
        cin>>s[i-1].rollno;
        cout<<"grade : ";
        cin>>s[i-1].grade;
        cout<<endl;
    }
    cout<<"the information of 3 students is as follows :"<<endl;
    cout<<"............................................."<<endl;
    for(int i=1;i<=3;i++){
        cout<<"student : "<<i<<endl;
        cout<<"name : "<<s[i-1].name<<endl;
        cout<<"rollno : "<<s[i-1].rollno<<endl;
        cout<<"grade : "<<s[i-1].grade<<endl<<endl;
    }
    return 0;
}
