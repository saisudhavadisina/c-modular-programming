#include <iostream>
using namespace std;
int k = 12;

namespace n1 {
    int x = 100;
}
namespace n2 {
    double x = 200;
}

int main() {
    int k = 13;
    cout << "n1 value : " << n1::x << endl;
    cout << "n2 value : " << n2::x << endl;
    cout << "local variable k : " << k << endl;   //prints the local variable:13
    cout << "global variable k : " << ::k << endl;   //prints global variable:12
    return 0;
}
