#include <stdio.h>
#include <stdlib.h>

typedef struct node{
    int data;
    struct node *prev;
    struct node *next;
} NODE1;

NODE1 *head = NULL;
//NODE1 *current = head;

void create();
void insert();
void delete();
void display();
void insert_begin();
void insert_end();
//void insert_at_position();
void delete_begin();
void delete_end();
//void insert_after_data();

int main(){
    int choice;
    while(1){
        printf("Enter your choice :\n1.Insert\n2.Delete\n3.Display\n4.Exit\n");
        scanf("%d", &choice);
        switch(choice){
            case 1:
                insert();
                break;
            /*case 2:
                delete();
                break;
            */case 3:
                display();
                break;
            case 4:
                exit(0);
        }
    }
}

void create(){
    NODE1 *p;
    p = malloc(sizeof(NODE1));
    int element;
    printf("Enter the element : ");
    scanf("%d", &element);
    p->data = element;
    p->prev = NULL;
    p->next = NULL;
    head->next = p;
}

void insert(){
    int option;
    while(1){
        printf("Enter the option:\n1.At Begin\n2.At End\n3.At Position\n4.After data\n5.Exit\n");
        scanf("%d", &option);
        switch(option){
            case 1:
                insert_begin();
                break;
            case 2:
                insert_end();
                break;
            /*case 3:
                insert_at_position();
                break;
            case 4:
                insert_after_data();
                break;*/
            case 5:
                exit(0);
        }
    }
}

void insert_begin(){
    if(head == NULL)
        create();
    else {
        NODE1 *p;
        p = malloc(sizeof(NODE1));
        int element;
        printf("Enter the element to be inserted : ");
        scanf("%d", &element);
        p->data = element;
        p->prev = NULL;
        p->next = head->next;
        head->next = p;
    }
}

void insert_end(){
    NODE1 *p;
    p = malloc(sizeof(NODE1));
    int element;
    printf("Enter the element to be inserted : ");
    scanf("%d", &element);
    p->data = element;
    p->prev = NULL;
    p->next = NULL;
    NODE1 *temp = head;
    while(temp->next != NULL)
        temp = temp->next;
    p->prev = temp->next;
    temp->next = p;
}

void delete(){
	int option;
    	while(1){
		printf("Enter the option:\n1.At Begin\n2.At End\n3.At Position\n4.After data\n5.Exit\n");
		scanf("%d", &option);
		switch(option){
			case 1:
				delete_begin();
				break;
			case 2:	
				delete_end();
				break;
			/*case 3:
				delete_at_position();
				break;
			case 4:
				delete_after_data();
				break;*/
			case 5:														                exit(0);
																        }
	}
}

void delete_begin(){
	NODE1 *temp = head;
	temp->next = head->next;
	head->next->prev = NULL;
	head->next = head->next->next;
	printf("Deleted element is : %d", temp->next->data);
	free(temp->next);
}
void delete_end(){
	NODE1 *temp = head;
	while(temp->next != NULL)
		temp->next = temp->next->next;
	temp->prev->next = NULL;
	temp->prev = NULL;
}
void display(){
    	NODE1 *temp = head;
    	if(head == NULL){
		printf("List doesn't exist\n");
    	} else {
    		printf("%d ", temp->next->data);
    		temp = temp->next;
    	}
}
