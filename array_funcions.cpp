#include<iostream>
using namespace std;
class person        //class of person
{
protected:
    char name[40];      //person name
public:
    void setName()      //set the name
    {
        cout << "Enter name:  ";
        cin >> name;
    }
    void printName()    //get the name
    {
        cout << "\n  Name is:  " << name;
    }
};

int main()
{
    person* persPtr[100];    //array of pointers to persons
    int n = 0;                //number of persons in array
    char choice;

    do
    {
        persPtr[n] = new person;        //make new object
        persPtr[n]->setName();            //set person name
        n++;    //count new person
        cout << "Enter another (y/n)? ";    //enter another?
        cin >> choice;
    } while( choice == 'y' );    //quit on 'n'

    for(int i = 0; i < n; i++)
    {
        cout << "\nPerson number " << i+1;
        persPtr[i]->printName();
    }
    cout << endl;
    return 0;
}
