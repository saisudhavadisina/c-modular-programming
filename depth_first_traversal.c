#include <stdio.h>

int n;
int a[10][10];
int v[10] = {0};

void dft(int i);
int main() {
    printf("Enter size of adjacancy matrix:");
    scanf("%d", &n);

    int start, f = -1, r = -1, j;

    printf("Enter adjacancy matrix:");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            scanf("%d", &a[i][j]);
        }
    }

    printf("Enter start :");
    scanf("%d", &start);

    dft( start);

    return 0;
}

void dft( int i) {
    printf("%d\t", i);
    v[i] = 1;

    for (int p = 0; p < n; p++) {
        if (a[i][p] == 1 && v[p] == 0) {
            dft(p);
        }
    }
}
