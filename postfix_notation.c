#include <stdio.h>
#include <string.h>
#include <stdbool.h>
char po[30];
char st[30];
char in[30];
int top=-1,m=0;
void push(char ch){
    top++;
    st[top] = ch;
}
char pop(){
    char t;
    t = st[top];
    top--;
    return t;
}
int precedence(char ch){
    switch(ch){
        case '*':
        case '/':
        case '%':return 2;
        case '+':
        case '-':return 1;
    }
}
bool isoperator(char ch){
    if(ch == '*' || ch == '/' || ch == '%' || ch == '+' || ch == '-'){
        return true;
    }
    else{
        return false;
    }
}
int main(){
  
    char c,k;
    printf("Enter infix notation:");
    scanf("%s",in);
    for(int i = 0;in[i] != '\0';i++){
        c = in[i];
        if(isoperator(c)){
            if(st[top] == '('){
                push(c);
            }
            else if(top != -1){
                while(precedence(c) <= precedence(st[top])){
                   k = pop();
                   po[m] = k;
                   m++;
                }
                push(c);
            }
            else{
                push(c);
            }
        }
        else if(c == '('){
            push(c);
        }
        else if(c == ')'){
            while(st[top] != '('){
               k = pop();
               po[m] = k;
               m++;
            }
            top--;
        }
        else{
            po[m]=c;
            m++;
        }
    }
    while(top != -1){
        k = pop();
        po[m] = k;m++;
    }
    po[m] = '\0';
    printf("The postfix notation:%s\n",po);
    return 0;
}



                   

